import XCTest

import luz_design_uiTests

var tests = [XCTestCaseEntry]()
tests += luz_design_uiTests.allTests()
XCTMain(tests)
