import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(luz_design_uiTests.allTests),
    ]
}
#endif
