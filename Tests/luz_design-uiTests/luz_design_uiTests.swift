import XCTest
@testable import luz_design_ui

final class luz_design_uiTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(luz_design_ui().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
