//
//  Typography.swift
//  KLARA UI
//
//  Created by Apple on 20/09/20.
//

import Foundation
import UIKit


class RobotoFonts {
    enum  size : Int {
        case h2 = 32
        case h5 = 16
        case h6 = 14
        case small = 12
    }
    enum style : String {
        case regular = "Roboto-Regular"
        case bold = "Roboto-Bold"
    }
}

extension UIFont {
    func robotoStyle(style : RobotoFonts.style , size : RobotoFonts.size) -> UIFont {
        return UIFont(name: style.rawValue, size: CGFloat(size.rawValue))!
    }
}
